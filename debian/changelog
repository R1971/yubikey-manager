yubikey-manager (4.0.9-1) unstable; urgency=medium

  * Team upload

  * New upstream version 4.0.9
  * Update maximum supported version of python3-cryptography and
    python3-fido2, require at least python3-click 7.0
  * Re-do the dir_to_symlink transition, to also catch systems closely
    following unstable (closes: #1012587)
  * Drop 0001-Fix-upstream-changelog.patch, no need to fix an old NEWS entry
  * Add missing Apache-2.0 licensed files to d/copyright
  * Declare compliance with Debian Policy 4.6.1

 -- Florian Schlichting <fsfs@debian.org>  Mon, 03 Oct 2022 22:22:24 +0200

yubikey-manager (4.0.7-1) unstable; urgency=medium

  * Team upload

  * Fix typo in long description
  * New upstream version 4.0.7
  * Recommend libyubikey-udev (closes: #994520)
  * Update patches, drop 0002 (applied upstream) and 0003 (setup.py now
    provided by upstream)
  * Drop debian/setup.py (obsolete)
  * Re-enable tests (closes: #981756)
  * Declare compliance with Debian Policy 4.6.0

 -- Florian Schlichting <fsfs@debian.org>  Mon, 15 Nov 2021 13:06:38 +0800

yubikey-manager (4.0.0~a1-4) unstable; urgency=medium

  * Revert lintian override from previous upload.
  * Add version to dependencies on fido2 and cryptography
    libraries.

 -- Taowa <taowa@debian.org>  Tue, 23 Mar 2021 17:00:00 -0500

yubikey-manager (4.0.0~a1-3) unstable; urgency=medium

  * Update maintscript to symlink doc directories for
    python3-yubikey-manager and yubikey-manager.
    Closes: #984480
  * Add myself as an uploader.

 -- Taowa <taowa@debian.org>  Sat, 20 Mar 2021 14:30:00 -0500

yubikey-manager (4.0.0~a1-2) unstable; urgency=medium

  * Cherry-pick upstream fix for OATH on Yubikey NEO
    Closes: #982533
  * python3-ykman breaks yubioath-desktop (<< 5.0.4+post1)
    Required due to API-breaking changes in v4.

 -- nicoo <nicoo@debian.org>  Thu, 11 Feb 2021 12:33:27 +0100

yubikey-manager (4.0.0~a1-1) unstable; urgency=high (RC)

  * New upstream version 4.0.0~a1
    + Supports fido2 0.9.x (Closes: #981428)
    + Adds NFC support to the FIDO and OTP functionality
    + Adds "ykman --diagnose" to aid in troubleshooting.
    + Restructures subcommands
      Aliases for old versions to be removed in a future release.
    + New module “yubikit” provides a high-level API for advanced users.

  * d/rules: Temporarily disable tests
    Some test dependencies are not yet packaged in Debian

  * Convert upstream's buildsystem to setuptools using dephell.
    This is necessary, as poetry is not yet packaged in Debian.
  * yubikey-manager: Depend on python3-ykman, not the transitional pkg
  * Upgrade to debhelper compatibility level 13
    `dh_missing --fail-missing` is now the default.

  * debian/copyright
    + Fix syntax of Upstream-Contact
    + Update for new version
  * d/control
    + Use a versioned relationship to the fido2 library
    + Declare compliance with policy v4.5.1.  (no change required)

 -- nicoo <nicoo@debian.org>  Wed, 03 Feb 2021 17:02:28 +0100

yubikey-manager (3.1.1-3) unstable; urgency=high (RC bug)

  * New sourceful upload to ensure migration to testing.
    No actual change, but necessary as:
    + Binary package python3-ykman was new (so a binary upload was necessary)
    + It is `Architecture: all` so a binNMU is not possible

 -- nicoo <nicoo@debian.org>  Mon, 02 Nov 2020 14:20:04 +0100

yubikey-manager (3.1.1-2) unstable; urgency=high (RC bug)

  * yubikey-manager: Avoid regenerating the manpage with help2man.
    Closes: #970806
  * Rename python3-yubikey-manager → python3-ykman
    In compliance with conventions on package names for Python modules.
  * Replace package docs with symlinks to python3-ykman's

  * d/copyright:
    + Set Upstream-Contact to Yubico's OSS maintainers role
    + Update package authorship metadata
  * d/patches: Update metadata
  * d/rules: Remove duplicate override_dh_installchangelogs

 -- nicoo <nicoo@debian.org>  Mon, 26 Oct 2020 12:54:54 +0100

yubikey-manager (3.1.1-1) unstable; urgency=medium

  * New upstream release (2020-01-29)
    + Add support for YubiKey 5C NFC

  * Declare compliance with policy v4.5.0.
    No change required.

  * debian/control: Build-Depends on libpcsclite1.
    This is a terrible hack, just so that `ykman` doesn't output errors
    when run under help2man, which then ends up in the manpage,

 -- nicoo <nicoo@debian.org>  Fri, 31 Jan 2020 03:48:14 +0100

yubikey-manager (3.1.0-1) unstable; urgency=medium

  * New upstream version 3.1.0
    + Add support for Yubikey 5Ci
    + PIV: Self-signed certificates now use UTC time
    + OTP: Support Norman keyboard layout

  * Update upstream's signing keys
  * Add missing build dependency on libykpers-1-dev

 -- nicoo <nicoo@debian.org>  Wed, 28 Aug 2019 14:42:07 +0200

yubikey-manager (3.0.0-3) unstable; urgency=low

  [ Alex Chernyakhovsky ]
  * debian/rules: Specify the C.UTF-8 locale under help2man (Closes: #926816)

  [ nicoo ]
  * debian/gbp.conf: Moved the packaging branch to debian/sid.
    This is consistent with DEP14, and allows us to have backport branches.
  * debian/changelog: Fix typo in entry v3.0.0-2

 -- nicoo <nicoo@debian.org>  Tue, 13 Aug 2019 02:08:51 +0200

yubikey-manager (3.0.0-2) unstable; urgency=medium

  * Add versioned dependency on python3-fido2.
    Mixing ykman 3.0.0 and python3-fido2 0.5.0 results in ImportError.

 -- nicoo <nicoo@debian.org>  Sat, 20 Jul 2019 14:30:11 +0200

yubikey-manager (3.0.0-1) unstable; urgency=low

  * New upstream version 3.0.0 (2019-06-24)

  * debian/upstream: Update keyring
  * debian/rules: Ship upstream's changelog
  * debian/control: Declare compliance with policy v4.4.0.
    No change required

 -- nicoo <nicoo@debian.org>  Sat, 20 Jul 2019 14:01:52 +0200

yubikey-manager (2.1.0-1) unstable; urgency=medium

  * New upstream release (2019-03-11)
    + PIV: Verify that the PIN must be between 6 - 8 characters long
    + PIV: Malformed certificates are now handled better
    + OpenPGP: The openpgp touch command now shows current touch policies
    + Bugfix: Fix support for german (DE) keyboard layout for static passwords
      Closes: #925212

  * debian/control: Add Afif Elghraoui as uploader

 -- nicoo <nicoo@debian.org>  Thu, 21 Mar 2019 12:30:40 +0100

yubikey-manager (2.0.0-2) unstable; urgency=low

  [ nicoo ]
  * Autogenerate a manpage with help2man.
    This is what upstream did, though doing it in the package build avoids
    having it go out-of-sync with the actual binary. (Closes: #912029)

  * Switch to debhelper 12.
    Compatibility level is now controlled through a Build-Depends

  [ Afif Elghraoui ]
  * d/control: add VCS URLs

 -- nicoo <nicoo@debian.org>  Sat, 09 Mar 2019 16:41:28 +0100

yubikey-manager (2.0.0-1) unstable; urgency=medium

  * New upstream version 2.0.0 (2019-01-09)
    + Add support for Yubikey 5 series (Closes: #911921)
    + Breaking API changes
    + Patch upstream changelog
      - Version 2.0.0 marked as unreleased
      - Mentioned a manpage that the official (signed) release tarball
        accidentally didn't include

  * Ship precompiled Python files
  * debian/rules
    + Do not skip tests
    + Make dh_missing fail the build

  * debian/control
    + Update uploader's email address  \o/
    + Declare compliance with v4.3.0.
      No change needed

 -- nicoo <nicoo@debian.org>  Thu, 07 Feb 2019 22:30:34 +0100

yubikey-manager (0.7.1-1) unstable; urgency=low

  * Initial package (upstream release 2018-07-09)
    Closes: #900939

 -- nicoo <nicoo@debian.org>  Fri, 03 Aug 2018 00:12:42 +0800
